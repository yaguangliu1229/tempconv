﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TempConv
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void rbtn_Checked(object sender, RoutedEventArgs e)
        {
            TempConver();
        }
        

        private void TempConver()
        {
            string inputTempStr = tempBox.Text;
            double temp;
            if (!double.TryParse(inputTempStr, out temp))
            {
                MessageBox.Show("Invalid input, please input again", "Error", MessageBoxButton.OK,
                    MessageBoxImage.Error);
            }

            if (btnCel.IsChecked == true)
            {
                if (btnCelResult.IsChecked == true)
                {
                    resultBlock.Text = inputTempStr + "° C";
                }else if (btnFarResult.IsChecked == true)
                {
                    var result = (9.0 / 5.0) * temp + 32;
                    resultBlock.Text = result.ToString("f2") + "° F";
                }else if (btnKelResult.IsChecked == true)
                {
                    var result = temp + 273.15;
                    resultBlock.Text = result.ToString("f2") + "° K";
                }
            }

            if (btnFar.IsChecked == true)
            {
                if (btnCelResult.IsChecked == true)
                {
                    var result = (5.0 / 9.0) * (temp - 32);
                    resultBlock.Text = result.ToString("f2") + "° C";
                }else if (btnFarResult.IsChecked == true)
                {
                    resultBlock.Text = inputTempStr + "° F";
                }else if (btnKelResult.IsChecked == true)
                {
                    var result = (5.0 / 9.0) * (temp - 32) + 273.15;
                    resultBlock.Text = result.ToString("f2") + "° K";
                }
            }

            if (btnKel.IsChecked == true)
            {
                if (btnCelResult.IsChecked == true)
                {
                    var result = temp - 273.15;
                    resultBlock.Text = result.ToString("f2") + "° C";
                }else if(btnFarResult.IsChecked == true)
                {
                    var result = (9.0 / 5.0) * (temp - 273.15) + 32;
                    resultBlock.Text = result.ToString("f2") + "° F";
                }else if (btnKelResult.IsChecked == true)
                {
                    resultBlock.Text = inputTempStr + "° K";
                }
            }
        }

        /*private void btnCelResult_Checked(object sender, RoutedEventArgs e)
        {
            string inputTempStr = tempBox.Text;
            double inputTemp;
            if (!double.TryParse(inputTempStr, out inputTemp))
            {
                MessageBox.Show("Your input is invalid, please input again", "Error", MessageBoxButton.OK,
                    MessageBoxImage.Error);
                
            }

            Console.WriteLine(inputTemp);
          
            if (btnCel.IsChecked == true)
            {
                resultBlock.Text = inputTempStr;
            }

            if (btnFar.IsChecked == true)
            {
                var result = (5.0/9.0 ) * (inputTemp - 32);
                //Console.WriteLine(result);
                resultBlock.Text = result.ToString("f2");
            }

            if (btnKel.IsChecked == true)
            {
                var result = inputTemp - 273;
                resultBlock.Text = result.ToString("f2");
            }
        }

        private void btnFarResult_Checked(object sender, RoutedEventArgs e)
        {
            string inputTempStr = tempBox.Text;
            double inputTemp;
            if (!double.TryParse(inputTempStr, out inputTemp))
            {
                MessageBox.Show("Your input is invalid, please input again", "Error", MessageBoxButton.OK,
                    MessageBoxImage.Error);

            }

            Console.WriteLine(inputTemp);

            if (btnCel.IsChecked == true)
            {
                var result = (9.0 / 5.0)* inputTemp+ 32;
                resultBlock.Text = result.ToString("f2");
            }

            if (btnFar.IsChecked == true)
            {
                resultBlock.Text = inputTempStr;
            }

            if (btnKel.IsChecked == true)
            {
                var result = (9.0/5.0) * (inputTemp - 273) + 32;
                resultBlock.Text = result.ToString("f2");
            }

        }

        private void btnKelResult_Checked(object sender, RoutedEventArgs e)
        {
            string inputTempStr = tempBox.Text;
            double inputTemp;
            if (!double.TryParse(inputTempStr, out inputTemp))
            {
                MessageBox.Show("Your input is invalid, please input again", "Error", MessageBoxButton.OK,
                    MessageBoxImage.Error);

            }

            Console.WriteLine(inputTemp);

            if (btnCel.IsChecked == true)
            {
                var result = inputTemp + 273;
                resultBlock.Text = result.ToString("f2");
            }

            if (btnFar.IsChecked == true)
            {
                var result = (5.0 / 9.0) * (inputTemp - 32) + 273;
                
                resultBlock.Text = result.ToString("f2");
            }

            if (btnKel.IsChecked == true)
            {
                resultBlock.Text = inputTempStr;
            }
        }*/

        private void tempBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            TempConver();
        }
    }
    
}